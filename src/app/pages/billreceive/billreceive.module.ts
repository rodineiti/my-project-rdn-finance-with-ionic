import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { BillReceivePageRoutingModule } from "./billreceive-routing.module";

import { BillReceivePage } from "./billreceive.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BillReceivePageRoutingModule
  ],
  declarations: [BillReceivePage]
})
export class BillReceivePageModule {}
