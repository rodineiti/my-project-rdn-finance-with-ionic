import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { BillReceivePage } from "./billreceive.page";

const routes: Routes = [
  {
    path: "",
    component: BillReceivePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillReceivePageRoutingModule {}
