import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/services/auth.service";
import * as Highcharts from "highcharts";
import { ReportService } from "src/app/services/report.service";
import { ToastService } from "src/app/services/toast.service";
import { LoadingController } from "@ionic/angular";
import * as moment from "moment";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.page.html",
  styleUrls: ["./dashboard.page.scss"]
})
export class DashboardPage implements OnInit {
  categoriesPay: any;
  categoriesReceive: any;
  statements: any;

  form: any = {
    dateStart: moment()
      .startOf("months")
      .format("YYYY-MM-DD"),
    dateEnd: moment()
      .endOf("months")
      .format("YYYY-MM-DD")
  };

  isResults: boolean = false;

  constructor(
    private authService: AuthService,
    private service: ReportService,
    public toast: ToastService,
    private loading: LoadingController
  ) {}

  ngOnInit() {
    this.submit();
  }

  submit() {
    if (this.form.dateStart !== null && this.form.dateEnd !== null) {
      this.presentLoading();
      this.form.dateStart = moment(this.form.dateStart).format('YYYY-MM-DD');
      this.form.dateEnd = moment(this.form.dateEnd).format('YYYY-MM-DD');
      this.service.getStatementByPeriod(this.form).subscribe(
        (res: any) => {
          this.closeLoading();
          const { statements } = res.data;
          this.statements = Object.values(statements) || [];
          this.isResults = true;
        },
        (err: any) => {
          this.closeLoading();
          this.toast.showErrorToast(err);
        }
      );

      this.service.sumChartsByPeriod(this.form).subscribe(
        (res: any) => {
          this.closeLoading();
          const { categoriesPay, categoriesReceive } = res.data;
          this.categoriesPay = categoriesPay;
          this.categoriesReceive = categoriesReceive;
          this.getChart("categoriesPay", "Pagos", this.categoriesPay);
          this.getChart(
            "categoriesReceive",
            "Recebidos",
            this.categoriesReceive
          );
          this.isResults = true;
        },
        (err: any) => {
          this.closeLoading();
          this.toast.showErrorToast(err);
        }
      );
    }
  }

  getChart(container: any, type: any, data: any) {
    Highcharts.chart(container, <any>{
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
      },
      title: {
        text: "Gráfico por período - " + type
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.y:.2f}</b>"
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [
        {
          name: "Brands",
          colorByPoint: true,
          data: data
        }
      ]
    });
  }

  logoutAction() {
    this.authService.logout();
  }

  async presentLoading() {
    const loading = await this.loading.create({
      message: "Aguarde..."
    });
    await loading.present();
  }

  async closeLoading() {
    await this.loading.dismiss();
  }
}
