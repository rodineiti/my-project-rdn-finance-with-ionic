import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { ToastService } from "src/app/services/toast.service";
import { AlertController, LoadingController } from "@ionic/angular";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.page.html",
  styleUrls: ["./signup.page.scss"]
})
export class SignupPage implements OnInit {
  data = {
    name: "",
    email: "",
    password: "",
    password_confirmation: ""
  };

  constructor(
    private router: Router,
    private authService: AuthService,
    private toastService: ToastService,
    private alertCtrl: AlertController,
    private loading: LoadingController
  ) {}

  ngOnInit() {}

  signup() {
    if (this.formValidate()) {
      this.presentLoading();
      this.authService.signup(this.data).subscribe(
        (response: any) => {
          this.closeLoading();
          if (response.status === "success") {
            this.presentConfirm(response.message);
          } else {
            this.toastService.showToast(
              "Erro ao tentar cadastrar, favor verificar"
            );
          }
        },
        (err: any) => {
          this.closeLoading();
          this.toastService.showErrorToast(err);
        }
      );
    } else {
      this.closeLoading();
      this.toastService.showToast(
        "Preencha todos os campos para completar o cadastro"
      );
    }
  }

  formValidate() {
    let email = this.data.email.trim();
    let name = this.data.name.trim();
    let password = this.data.password.trim();
    let password_confirmation = this.data.password_confirmation.trim();

    return (
      this.data.email &&
      this.data.password &&
      this.data.name &&
      this.data.password_confirmation &&
      email.length &&
      password.length &&
      name.length &&
      password_confirmation.length
    );
  }

  async presentConfirm(message: string) {
    let alert = await this.alertCtrl.create({
      message: message,
      buttons: [
        {
          text: "OK",
          handler: () => {
            this.router.navigate(["/login"]);
          }
        }
      ]
    });
    alert.present();
  }

  async presentLoading() {
    const loading = await this.loading.create({
      message: "Aguarde..."
    });
    await loading.present();
  }

  async closeLoading() {
    await this.loading.dismiss();
  }
}
