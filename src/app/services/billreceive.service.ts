import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { StorageService } from "./storage.service";

@Injectable({
  providedIn: "root"
})
export class BillreceiveService {
  private url = environment.API_URL;
  public token: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.getToken();
  }

  index() {
    console.log("bill_receives.index");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.get(this.url + "/api/v1/bill_receives", options);
  }

  store(data: any) {
    console.log("bill_receives.store");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.post(
      this.url + "/api/v1/bill_receives/store",
      data,
      options
    );
  }

  show(id: Number) {
    console.log("bill_receives.show");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.get(
      this.url + "/api/v1/bill_receives/show/" + id,
      options
    );
  }

  update(data: any, id: Number) {
    console.log("bill_receives.update");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.put(
      this.url + "/api/v1/bill_receives/update/" + id,
      data,
      options
    );
  }

  destroy(id: Number) {
    console.log("bill_receives.destroy");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.token,
      Accept: "application/json"
    });
    const options = { headers: headers };

    return this.http.delete(
      this.url + "/api/v1/bill_receives/destroy/" + id,
      options
    );
  }

  getToken() {
    const token =
      this.storageService.getLocalStorage(environment.AUTH_TOKEN) || null;
    if (token) {
      this.token = token.access_token;
    }
  }
}
