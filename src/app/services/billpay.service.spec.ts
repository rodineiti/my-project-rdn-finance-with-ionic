import { TestBed } from '@angular/core/testing';

import { BillpayService } from './billpay.service';

describe('BillpayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BillpayService = TestBed.get(BillpayService);
    expect(service).toBeTruthy();
  });
});
