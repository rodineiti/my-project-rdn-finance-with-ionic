import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, BehaviorSubject } from "rxjs";
import { HttpService } from "./http.service";
import { StorageService } from "./storage.service";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  userData$ = new BehaviorSubject<any>([]);

  constructor(
    private httpService: HttpService,
    private storageService: StorageService,
    private router: Router
  ) {}

  login(data: any): Observable<any> {
    let body = {
      grant_type: "password",
      client_id: environment.client_id,
      client_secret: environment.client_secret,
      username: data.email,
      password: data.password,
      scope: ""
    };

    return this.httpService.post("/oauth/token", body);
  }

  signup(data: any): Observable<any> {
    return this.httpService.post("/api/v1/auth/register", data);
  }

  getUser() {
    this.storageService.get(environment.AUTH_TOKEN).then(response => {
      this.userData$.next(response);
    });
  }

  logout() {
    this.storageService.removeLocalStorage(environment.AUTH_TOKEN);
    this.storageService
      .removeStorageItem(environment.AUTH_TOKEN)
      .then(response => {
        this.userData$.next("");
        this.router.navigate(["/"]);
      });
  }
}
