import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { StorageService } from "../services/storage.service";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class HomeGuard implements CanActivate {
  constructor(public storageService: StorageService, public router: Router) {}
  canActivate(): Promise<boolean> {
    return new Promise(resolve => {
      this.storageService
        .get(environment.AUTH_TOKEN)
        .then(response => {
          if (!response) {
            this.router.navigate(["/login"]);
            resolve(false);
          }

          resolve(true);
        })
        .catch(error => {
          resolve(false);
        });
    });
  }
}
